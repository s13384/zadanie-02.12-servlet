package TableModels;

public class ConferenceInfo {

	private ConfInfo option;
	private String othersWhich;

	public ConferenceInfo() {

	}

	public ConferenceInfo(ConfInfo option, String othersWhich) {
		this.option = option;
		this.othersWhich = othersWhich;
	}

	public ConfInfo getOption() {
		return option;
	}

	public void setOption(ConfInfo option) {
		this.option = option;
	}

	public String getOthersWhich() {
		if (option == ConfInfo.other)
			return othersWhich;
		else
			return "";
	}

	public void setOthersWhich(String othersWhich) {
		this.othersWhich = othersWhich;
	}
}
