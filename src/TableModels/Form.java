package TableModels;

public class Form {

	private String firstName;
	private String lastName;
	private String email;
	private String employer;
	private ConferenceInfo conferenceInfo;
	private String hobbies;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmployer() {
		return employer;
	}
	public void setEmployer(String employer) {
		this.employer = employer;
	}
	public ConferenceInfo getConferenceInfo() {
		return conferenceInfo;
	}
	public void setConferenceInfo(ConferenceInfo conferenceInfo) {
		this.conferenceInfo = conferenceInfo;
	}
	public String getHobbies() {
		return hobbies;
	}
	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}
}
