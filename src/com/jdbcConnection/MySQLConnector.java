package com.jdbcConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import TableModels.Form;

public class MySQLConnector {

	private Connection conn = null;
	private PreparedStatement selectFormWithFirstNameAndLastNameAndEmail = null;
	private PreparedStatement insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies = null;
	private PreparedStatement selectCount = null;
	private int maxForms;

	public MySQLConnector(int maxForms) {
		this.maxForms = maxForms;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/feedback?" + "user=sqluser&password=sqluserpw");
			selectFormWithFirstNameAndLastNameAndEmail = conn.prepareStatement(
					"Select First_Name, Last_Name, Email from t_sys_forms where First_Name = ? and Last_Name = ? and Email = ?");
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies = conn
					.prepareStatement(
							"Insert into t_sys_forms ( First_Name, Last_Name, Email, Employer, ConferenceInfoOption, ConferenceInfoOthersWhich, Hobbies) values (?, ?, ?, ?, ?, ?, ?)");
			selectCount = conn.prepareStatement("Select count (*) from t_sys_forms");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean formExistsInDataBase(Form form) {
		boolean formExists = false;
		try {
			selectFormWithFirstNameAndLastNameAndEmail.setString(1, form.getFirstName());
			selectFormWithFirstNameAndLastNameAndEmail.setString(2, form.getLastName());
			selectFormWithFirstNameAndLastNameAndEmail.setString(3, form.getEmail());
			ResultSet rs = selectFormWithFirstNameAndLastNameAndEmail.executeQuery();
			while (rs.next()) {
				if (formWasFound(form, rs)) {
					formExists = true;
				}
			}
			return formExists;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public int numberOfFormsRegistered() {
		// max forms jest po to ze jak sie cos zepsuje to nie bedzie dodawac
		// nowych formularzy
		int fromsRegistered = maxForms;
		try {
			ResultSet rows = selectCount.executeQuery();

			while (rows.next()) {
				fromsRegistered = rows.getInt("count");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return fromsRegistered;
	}

	private boolean formWasFound(Form form, ResultSet rs) throws SQLException {

		if (form.getFirstName() == rs.getString("First_Name") && form.getLastName() == rs.getString("Last_Name")
				&& form.getEmail() == rs.getString("Email")) {
			return true;
		}

		return false;
	}

	public void insertForm(Form form) {
		try {
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(1, form.getFirstName());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(2, form.getLastName());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(3, form.getEmail());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(4, form.getEmployer());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(5, form.getConferenceInfo().getOption().toString());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(6, form.getConferenceInfo().getOthersWhich());
			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.setString(7, form.getHobbies());

			insertFormWithFirstNameAndLastNameAndEmailAndEmplyerAndConferenceInfoOptionAndConferenceInfoOthersWhichAndHobbies
					.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
