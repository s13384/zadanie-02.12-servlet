package pl.javastart.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jdbcConnection.MySQLConnector;

import TableModels.ConfInfo;
import TableModels.ConferenceInfo;
import TableModels.Form;

public class Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final int maxForms = 5;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		MySQLConnector database = new MySQLConnector(maxForms);

		Form form = buildFormFromRequest(request);

		if (database.formExistsInDataBase(form)) {
			setResponseFormExists(response);
		} else {
			if (database.numberOfFormsRegistered() < maxForms) {
				database.insertForm(form);
				setResponseFormInserted(response);
			} else {
				setResponseTooManyForms(response);
			}
		}

	}

	private Form buildFormFromRequest(HttpServletRequest request) {
		Form form = new Form();
		form.setFirstName(request.getParameter("First_Name"));
		form.setLastName(request.getParameter("Last_Name"));
		form.setEmail(request.getParameter("Email"));
		form.setEmployer(request.getParameter("Employer"));
		ConferenceInfo confInfo = new ConferenceInfo();
		confInfo.setOption(optionFromRequest(request));
		confInfo.setOthersWhich(request.getParameter("OthersWhich"));
		form.setConferenceInfo(confInfo);
		form.setHobbies(request.getParameter("Hobbies"));
		return form;
	}

	private ConfInfo optionFromRequest(HttpServletRequest request) {
		String option = request.getParameter("Option");
		if (option == ConfInfo.facebook.toString()) {
			return ConfInfo.facebook;
		} else if (option == ConfInfo.friends.toString()) {
			return ConfInfo.friends;
		} else if (option == ConfInfo.job_ad.toString()) {
			return ConfInfo.job_ad;
		} else if (option == ConfInfo.university_ad.toString()) {
			return ConfInfo.university_ad;
		}else {
			return ConfInfo.other;
		}
	}

	private void setResponseFormInserted(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Formularz przyjety");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setResponseFormExists(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Ta osoba juz sie zarejestrowala");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setResponseTooManyForms(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Niestety mamy juz za duzo formularzy");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}